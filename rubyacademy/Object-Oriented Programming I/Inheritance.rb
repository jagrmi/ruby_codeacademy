class Application
  def initialize(name)
    @name = name
  end
end

class MyApp < Application
  #
end

# Override
class Creature
  def initialize(name)
    @name = name
  end

  def fight
    return "Punch to the chops!"
  end
end

# Add your code below!

class Dragon < Creature
  def fight
    puts "Instead of breathing fire..."
    super()
  end
end
