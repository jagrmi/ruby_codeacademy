# BLOCK
odds_n_ends = [:weezard, 42, "Trady Blix", 3, true, 19, 12.345]
ints = odds_n_ends.select {|num| num.is_a? Integer}
puts "BLOCK"
print ints, ""
puts ""
# PROC

ages = [23, 101, 7, 104, 11, 94, 100, 121, 101, 70, 44]

# Add your code below!
under_100 = Proc.new {|num| num < 100}
youngsters = ages.select(&under_100)
puts "PROC"
print youngsters , ""
puts ""


# LAMBDA
crew = {
    captain: "Picard",
    first_officer: "Riker",
    lt_cdr: "Data",
    lt: "Worf",
    ensign: "Ro",
    counselor: "Troi",
    chief_engineer: "LaForge",
    doctor: "Crusher"
}
# Add your code below!
first_half = lambda {|key, value| value < "M"}
a_to_m = crew.select(&first_half)
puts "LAMBDA"
print a_to_m, ""
