def lambda_demo(a_lambda)
  puts "I'm the method!"
  a_lambda.call
end

lambda_demo(lambda { puts "I'm the lambda!" })


# Lambdas are defined using the following syntax:
lambda { |param| block }


strings = ["leonardo", "donatello", "raphael", "michaelangelo"]
# Write your code below this line!

symbolize = lambda {|param| param.to_sym}

# Write your code above this line!
symbols = strings.collect(&symbolize)
puts symbols