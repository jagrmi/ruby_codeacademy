=begin
First, a lambda checks the number of arguments passed to it, while a proc does not.
This means that a lambda will throw an error if you pass it the wrong number of arguments, whereas a proc will ignore
unexpected arguments and assign nil to any that are missing.

Second, when a lambda returns, it passes control back to the calling method; when a proc returns,
it does so immediately, without going back to the calling method.
=end

def batman_ironman_proc
  victor = Proc.new { return "Batman will win!" }
  victor.call
  "Iron Man will win!"
end

puts batman_ironman_proc
puts '1'

def batman_ironman_lambda
  victor = lambda { return "Batman will win!" }
  victor.call
  "Iron Man will win!"
end

puts batman_ironman_lambda


=begin
.is_a? method, which returns true if an object is the type of object named and false otherwise:

:hello.is_a? Symbol
# ==> true
=end


my_array = ["raindrops", :kettles, "whiskers", :mittens, :packages]
symbol_filter = lambda {|param| param.is_a? Symbol}
symbols = my_array.select(&symbol_filter)

print symbols, " "
puts ""
