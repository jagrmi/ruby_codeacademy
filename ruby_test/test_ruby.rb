require 'open-uri'
require 'nokogiri'
require 'csv'

def ruby_test(url, file)
  html = open(url)
  url_base = url
  count_paginator = 1
  doc = Nokogiri::HTML(html)
  count_pag_all = doc.xpath("//ul[@class='pagination pull-left']/li/a")
  array_a = Array.new

  # Calculate the number of pages of products
  count_pag_all.each {|elemnt| array_a.push( elemnt['href'].to_s.strip.split('?p=')[1].to_i)}
  count_loop = 0
  paginate_item = array_a.max

  # if one page products
  unless paginate_item
    paginate_item = 1
  end
  for i in 1..paginate_item.to_i
    html = open(url)
    doc = Nokogiri::HTML(html)

    # Collect names and links to the product
    # tagcloud_elements = doc.xpath("//a[@class='product_img_link']")
    tagcloud_elements = doc.xpath("//div[@class='productlist']//a[@class='product_img_link']")
    tagcloud_elements.each do |tagcloud_element|
      count_loop += 1
      url_product =  tagcloud_element['href']
      text_product = tagcloud_element['title']
      html_product = open(url_product)
      doc_product = Nokogiri::HTML(html_product)

      # link image
      image_product = doc_product.xpath("//span[@id='view_full_size']/img").to_s.split('src="')[1].to_s.split('"')[0]
      tagcloud_element_products = doc_product.xpath("//span[@class='attribute_price']")
      count = 0
      tagcloud_element_products.each do |tagcloud_element_product|
        count += 1
        price = tagcloud_element_product.text.strip.split(' ')[0]
        attribute_name = tagcloud_element_product.parent.xpath("span[@class='attribute_name']").text.strip
        # puts "Price equel - #{count} - #{price}, weight - #{attribute_name}"

        # Tile + weight produts
        new_title = text_product + " " + attribute_name

        # recording file
        CSV.open(file,"a") do |wr|
          wr <<[new_title, price, image_product]
        end
      end
    end

    # Create next page (pagination)
    count_paginator += 1
    url = url_base + "?p=" + count_paginator.to_s
  end
  puts "Finish work. #{count_loop} - loops!!!"
end
# 12 pages
ruby_test('https://www.petsonic.com/snacks-huesos-para-perros/', 'data_ex.csv')
# two pages
# ruby_test('https://www.petsonic.com/pienso-purina-proplan-para-perros/', 'data_3.csv')
# five pages
# ruby_test('https://www.petsonic.com/pienso-royal-canin-para-perros/', 'data_4.csv')
# one page
# ruby_test('https://www.petsonic.com/pienso-true-instinct-para-perros/', 'data_6.csv')
# two page
# ruby_test('https://www.petsonic.com/bocaditos-para-gato/', 'data_7.csv')
# 18 pages
# ruby_test('https://www.petsonic.com/pienso-para-gatos/', 'data_8.csv')
# 64 pages
# ruby_test('https://www.petsonic.com/tienda-gatos/', 'data_9.csv')