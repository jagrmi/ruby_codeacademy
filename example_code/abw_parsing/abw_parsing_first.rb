require 'open-uri'
require 'nokogiri'
require 'csv'

def abw_ruby_parsing(url, file)
  url_base = url
  count_paginator = 1

  loop do
    # Loop until there is next page
    html_pagination = open(url)
    doc_pagination = Nokogiri::HTML(html_pagination)
    tagcloud_true = doc_pagination.xpath("//div[@class='pagination clearfix']//a[@class='next']").css('.next').text
    # Each page with pagination
    html = open(url)
    doc = Nokogiri::HTML(html)
    tagcloud_elements = doc.xpath("//div[@class='prosucts-list']//div[@class='product-full-inner']")
    tagcloud_elements.each do |tagcloud_element|
      url_product = tagcloud_element.at_css('.main-link')['href']
      mileage_html = tagcloud_element.at_css('.mileage')
      mileage = mileage_html.text
      year = mileage_html.parent.at_css('span').text
      # Go to car page
      html_each = open(url_product)
      doc_each = Nokogiri::HTML(html_each)
      tagcloud_each_elements = doc_each.xpath("//div[@class='product-main-data']")
      tagcloud_each_elements.each do |tagcloud_each_element|
        price_byn = tagcloud_each_element.css('.price-byn').text
        title_auto = tagcloud_each_element.css('.title').text
        # recording file
        CSV.open(file,"a") do |wr|
          wr <<[title_auto, price_byn, mileage, year]
        end
      end

    end
    count_paginator += 1
    url = url_base + "?page=" + count_paginator.to_s
    break if tagcloud_true == ''
  end
end

url = 'https://www.abw.by/car/sell/honda/accord/'
abw_ruby_parsing(url, 'honda_accord.csv')